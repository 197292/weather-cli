import axios from "axios";

import { printError } from "./log-service.js";
import { getKeyValue } from "./storage-service.js";

export const getWeatherByCity = async (city) => {
  const token = process.env.TOKEN ?? (await getKeyValue("token"));
  if (!token) {
    throw new Error("Token is not set, set token over -t [TOKEN]");
  }

  try {
    const { data } = await axios.get(
      "https://api.openweathermap.org/data/2.5/weather",
      {
        params: {
          appid: token,
          q: city,
          lang: "ru",
          units: "metric",
        },
      }
    );
    return data;
  } catch (err) {
    printError(err.message);
    throw err;
  }
};

export const getIcon = (icon) => {
  switch (icon.slice(0, -1)) {
    case "01":
      return "☀️";
    case "02":
      return "🌤️";
    case "03":
      return "☁️";
    case "04":
      return "☁️";
    case "09":
      return "🌧️";
    case "10":
      return "🌦️";
    case "11":
      return "🌩️";
    case "13":
      return "❄️";
    case "50":
      return "🌫️";
  }
};
