import chalk from "chalk";
import dedent from "dedent-js";

export const printError = (error) => {
  console.log(chalk.bgRed(" ERROR ") + " " + error);
};

export const printSuccess = (message) => {
  console.log(chalk.bgGreen(" SUCCESS ") + " " + message);
};

export const printHelp = () => {
  console.log(
    dedent(`${chalk.bgCyan(" HELP ")}
    Output weather if arguments is absent
    -city [CITY] - set a city
    -t [TOKEN] - set an API TOKEN
    -h - output a help menu
  `)
  );
};

export const printWeather = (data = {}, icon) => {
  const { name, main, weather, wind } = data;

  console.log(
    dedent(`${chalk.bgYellow.black(" WEATHER FORECAST ")} The weather of ${name}
    ${icon} ${weather[0].description}
    Temperature: ${main.temp}
    Temperature feels like: ${main.feels_like}
    Wind speed (metres): ${wind.speed}
    Pressure: ${main.pressure}
    Temperature min: ${main.temp_min}
    Temperature max: ${main.temp_max}
  `)
  );
};
