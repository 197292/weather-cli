import yargs from "yargs";
import { hideBin } from "yargs/helpers";

import { getIcon, getWeatherByCity } from "./services/api-service.js";
import {
  printError,
  printHelp,
  printSuccess,
  printWeather,
} from "./services/log-service.js";
import { getKeyValue, saveKeyValue } from "./services/storage-service.js";

const saveToken = async (token) => {
  if (!token) {
    printError("Token doesn't passed");
    return;
  }

  if (typeof token !== "string") {
    printError("Token should be a string");
    return;
  }

  try {
    await saveKeyValue("token", token);
    printSuccess("Token is saved");
    return;
  } catch (error) {
    printError(error.message);
  }
};

const saveCity = async (city) => {
  if (!city) {
    printError("City doesn't passed");
    return;
  }

  if (typeof city !== "string") {
    printError("Token should be a string");
    return;
  }

  try {
    await saveKeyValue("city", city);
    printSuccess("City is saved");
    return;
  } catch (error) {
    printError(error.message);
  }
};

const getForecast = async () => {
  try {
    const city = process.env.CITY ?? (await getKeyValue("city"));
    const weather = await getWeatherByCity(city);
    printWeather(weather, getIcon(weather.weather[0].icon));
  } catch (e) {
    if (e?.response?.status === 404) {
      printError("City is wrong");
    } else if (e?.response?.status === 401) {
      printError("Token is wrong");
    } else {
      printError(e.message);
    }
  }
};

const initCLI = async () => {
  const args = yargs(hideBin(process.argv)).argv;

  if (args.h) {
    printHelp();
    return;
  }
  if (args.city) {
    saveCity(args.city);
    return;
  }
  if (args.t || args.token) {
    saveToken(args.t || args.token);
    return;
  }
  await getForecast();
};

initCLI();
